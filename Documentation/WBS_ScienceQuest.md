```plantuml
@startwbs
<style>
    wbsDiagram {
        .blue {
            BackgroundColor lightblue
        }
        .green {
            BackgroundColor lightgreen
        }
        .yellow {
            BackgroundColor #ffbf87
        }
        .orange {
            BackgroundColor lightyellow
        }
        .red {
            BackgroundColor #FFB2B2
        }
        .pink {
            BackgroundColor #ff99ee
        }
        .purple {
            BackgroundColor #cfb3ff
        }
    }
</style>

* Projet "Science Quest" <<blue>>
** Établir la conception <<green>>
*** Créer le site Web <<yellow>>
**** Conception des mini-jeux <<orange>>
***** Conception du jeu "Qui-est-ce ?" <<red>>
***** Conception du jeu "Quizz" <<red>>
***** Conception du jeu "Pendu" <<red>>
*** Créer l'application Mobile <<yellow>>
**** Conception des mini-jeux <<orange>>
***** Conception du jeu "Qui-est-ce ?" <<red>>
***** Conception du jeu "Quizz" <<red>>
***** Conception du jeu "Pendu" <<red>>
** Démarrer le développement <<green>>
*** Développer le site Web <<yellow>>
**** Développement des mini-jeux <<orange>>
***** Développement du jeu "Qui-est-ce ?" <<red>>
***** Développement du jeu "Quizz" <<red>>
***** Développement du jeu "Pendu" <<red>>
*** Développer l'application Mobile <<yellow>>
**** Développement des mini-jeux <<orange>>
***** Développement du jeu "Qui-est-ce ?" <<red>>
***** Développement du jeu "Quizz" <<red>>
***** Développement du jeu "Pendu" <<red>>
** Tester l'application <<green>>
*** Tester le site Web <<yellow>>
**** Test des mini-jeux <<orange>>
***** Test du jeu "Qui-est-ce ?" <<red>>
***** Test du jeu "Quizz" <<red>>
***** Test du jeu "Pendu" <<red>>
*** Tester l'application Mobile <<yellow>>
**** Test des mini-jeux <<orange>>
***** Test du jeu "Qui-est-ce ?" <<red>>
***** Test du jeu "Quizz" <<red>>
***** Test du jeu "Pendu" <<red>>
** Création des livrables <<pink>>
*** Application client web <<purple>>
*** Application client mobile <<purple>>
*** Application admin web <<purple>>
*** Application app monitoring web <<purple>>
*** Conception et contenu de la base de données <<purple>>
*** Installation d’un pare-feu <<purple>>
*** Installation d’un service web <<purple>>
*** Installation d’un SGBD <<purple>>
*** Installation des serveurs pour les services nommées ci-dessus et configurations <<purple>>
@endwbs